package com.aiden.diploma_system.utils;

import org.springframework.stereotype.Component;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.admin.methods.response.NewAccountIdentifier;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterNumber;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.tx.Contract;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import org.web3j.protocol.geth.Geth;

@Component
public class SmartContract {

    private String source = "C:\\Users\\徵羽\\AppData\\Roaming\\Ethereum\\keystore\\UTC--2020-03-16T12-28-20.000750700Z--37f4753d6f23c9b7065e7271b8f90f8f9efb8cd8";
    public static Web3j web3j;
    public static Admin admin;
    public static Geth geth;
    private static String password="123456";
    public Credentials credentials;
    public String ContractAddress;

    public void minerStart() throws IOException {
        geth.minerStart(1).send();
    }

    public void minerStop() throws IOException {
        geth.minerStop().send();
    }

    //在此处更改智能合约地址
    public SmartContract() throws Exception{
        credentials = WalletUtils.loadCredentials(password, source);
        web3j = Web3JClient.getClient();
        admin = Web3JClient.getAdmin();
        geth = Web3JClient.getGeth();
        ContractAddress="0x3d2205e4a2ab2c30e7289517347d65f8d96870e8";
    }

    public Diploma deployContract()throws Exception {
        Diploma diploma = Diploma.deploy(web3j,credentials,Contract.GAS_PRICE, Contract.GAS_LIMIT).send();
        return  diploma;
    }

    //加载智能合约
    public Diploma loadContract()  {
        Diploma diploma = Diploma.load(ContractAddress, web3j, credentials,  new BigInteger("21000"), new BigInteger("35000000000"));
        return diploma;
    }

    //注册账户
    public String Regist()throws Exception {
        String walletFileName="";//文件名
        String walletFilePath="C:\\Users\\徵羽\\AppData\\Roaming\\Ethereum\\keystore";
        //钱包文件保持路径，请替换位自己的某文件夹路径
        walletFileName = WalletUtils.generateNewWalletFile("123456", new File(walletFilePath), false);
        Credentials new_credentials = WalletUtils.loadCredentials(password, walletFilePath + "\\" + walletFileName);
        String address = new_credentials.getAddress();
        return address;
    }

    public List<String> getAccountlist() {
        try {
            return admin.personalListAccounts().send().getAccountIds();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String createAccount() {
        try {
            NewAccountIdentifier newAccountIdentifier = admin.personalNewAccount(password).send();
            if (newAccountIdentifier != null) {
                String accountId = newAccountIdentifier.getAccountId();
//                admin.personalSetAccountName(accountId,accountName);
//                admin.
//
//                Map<String,Object> account = new HashMap<String,Object>();
//                account.put(accountId,accountInfo);
//                parity.personalSetAccountMeta(accountId,account);account

                return accountId;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    public PersonalAccountsInfo.AccountsInfo getAccountInfo(String accountId){
//
//        try{
//            PersonalAccountsInfo personalAccountsInfo = parity.personalAccountsInfo().send();
//
//            return  personalAccountsInfo.getAccountsInfo().get(accountId);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }

    public BigInteger getBalance(String accountId) {
        try {
            DefaultBlockParameter defaultBlockParameter = new DefaultBlockParameterNumber(web3j.ethBlockNumber().send().getBlockNumber());
            EthGetBalance ethGetBalance = web3j.ethGetBalance(accountId, defaultBlockParameter).send();
            if (ethGetBalance != null) {
                return ethGetBalance.getBalance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
